﻿using UnityEngine;

public class TowerNumberLabel : MonoBehaviour
{
    [SerializeField] TMPro.TMP_Text text = default;

    private void OnEnable()
    {
        TowerBehaviour.TowerCountModified += OnTowerCountModified;
        RefreshText();
    }

    private void OnDisable()
    {
        TowerBehaviour.TowerCountModified -= OnTowerCountModified;
    }

    private void RefreshText()
    {
        text.text = TowerBehaviour.TowerCount.ToString();
    }

    private void OnTowerCountModified()
    {
        RefreshText();
    }
}
