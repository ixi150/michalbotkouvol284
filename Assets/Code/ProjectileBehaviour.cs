﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour
{
    [SerializeField] TowerBehaviour towerPrefab = default;
    [SerializeField] float speed = 4f;
    [SerializeField] Vector2 randomRange = new Vector2(1, 4);

    bool initialized;
    TowerBehaviour owner;
    Rigidbody2D rb;

    private void Awake()
    {
        TryGetComponent(out rb);
    }

    private void OnDisable()
    {
        initialized = false;
        owner = null;
    }

    public void Initialize(TowerBehaviour owner)
    {
        this.owner = owner;
        rb.velocity = transform.up * speed;
        var range = Random.Range(randomRange.x, randomRange.y);
        var lifetime = range / speed;
        StartCoroutine(SpawnTowerAfterDelay(lifetime));
        initialized = true;
    }

    IEnumerator SpawnTowerAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        if (TowerBehaviour.TowerCount < TowerBehaviour.MaxTowers)
        {
            var tower = ObjectPooling.Ref.GetOrCreate(towerPrefab, transform.position);
        }

        ObjectPooling.Ref.ReturnInstanceToPool(this);
    }

    private void OnTriggerEnter2D(Collider2D other)
    { 
        if (initialized)
        {
            var tower = other.GetComponent<TowerBehaviour>();
            if (tower != null && tower != owner)
            {
                tower.Deactivate();
                ObjectPooling.Ref.ReturnInstanceToPool(tower);
                ObjectPooling.Ref.ReturnInstanceToPool(this);
            }
        }
    }
}
