﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooling : MonoBehaviour
{
    public static ObjectPooling Ref { get; private set; }
    Transform container;

    Dictionary<GameObject, Queue<Object>> pool = new Dictionary<GameObject, Queue<Object>>();
    Dictionary<Object, GameObject> instanceToPrefab = new Dictionary<Object, GameObject>();

    private void Awake()
    {
        Ref = this;
        DontDestroyOnLoad(gameObject);
        container = new GameObject("PoolContainer").transform;
        container.SetParent(transform);
        container.gameObject.SetActive(false);
    }

    public T GetOrCreate<T>(T prefab, Vector3 position = default, Quaternion rotation = default, Transform parent = null) where T : MonoBehaviour
    {
        var key = prefab.gameObject;
        var queue = GetOrCreateQueue(key);
        if (queue.Count > 0)
        {
            T obj = queue.Dequeue() as T;
            var t = obj.transform;
            t.position = position;
            t.rotation = rotation;
            t.SetParent(parent);
            return obj;
        }

        var newInstance = Instantiate(prefab, position, rotation, parent);
        instanceToPrefab[newInstance] = prefab.gameObject;
        return newInstance;
    }

    public void ReturnInstanceToPool<T>(T instance) where T : MonoBehaviour
    {
        var key = instanceToPrefab[instance];
        var queue = GetOrCreateQueue(key);
        queue.Enqueue(instance);
        instance.transform.SetParent(container);
    }

    private Queue<Object> GetOrCreateQueue(GameObject key)
    {
        if (!pool.TryGetValue(key, out var queue))
        {
            queue = new Queue<Object>();
            pool[key] = queue;
        }

        return queue;
    }
}
