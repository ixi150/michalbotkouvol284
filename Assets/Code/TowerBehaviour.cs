﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehaviour : MonoBehaviour
{
    [SerializeField] Color activeColor = Color.red;
    [SerializeField] Color inactiveColor = Color.white;
    [SerializeField] SpriteRenderer[] renderers = default;
    [SerializeField] float timeIntervalBetweenShots= .5f;
    [SerializeField] int maxAmmo = 12;
    [SerializeField] Vector2 rotationAngleRandomRange = new Vector2(15, 45);
    [SerializeField] ProjectileBehaviour projectilePrefab = default;

    bool active;
    int ammo;
    Transform cachedTransform;
    YieldInstruction waitingYieldInstruction;

    public const int MaxTowers = 100;
    public static int TowerCount { get; private set; }
    public static event Action TowerCountModified;

    private void Awake()
    {
        waitingYieldInstruction = new WaitForSeconds(timeIntervalBetweenShots);
    }

    private void OnEnable()
    {
        cachedTransform = transform;
        Activate();
        TowerCount++;
        TowerCountModified?.Invoke();
        TowerCountModified += OnTowerCountModified;
    }

    private void OnDisable()
    {
        TowerCount--;
        TowerCountModified?.Invoke();
        TowerCountModified -= OnTowerCountModified;
    }

    public void Activate()
    {
        SetColor(activeColor);
        ammo = maxAmmo;
        if (!active)
        {
            StartCoroutine(ShootingRoutine());
        }

        active = true;
    }

    public void Deactivate()
    {
        StopAllCoroutines();
        ammo = 0;
        SetColor(inactiveColor);
        active = false;
    }

    IEnumerator ShootingRoutine()
    {
        while (ammo > 0)
        {
            yield return waitingYieldInstruction;
            Rotate();
            Shoot();
        }
    }

    private void Rotate()
    {
        var angle = UnityEngine.Random.Range(rotationAngleRandomRange.x, rotationAngleRandomRange.y);
        cachedTransform.Rotate(0, 0, angle);
    }

    private void Shoot()
    {
        if (ammo <= 0)
        {
            return;
        }

        ammo--;
        var projectile = ObjectPooling.Ref.GetOrCreate(projectilePrefab, cachedTransform.position, cachedTransform.rotation);
        projectile.Initialize(this);

        if (ammo <= 0)
        {
            Deactivate();
        }
    }

    private void SetColor(Color color)
    {
        foreach (var renderer in renderers)
        {
            renderer.color = color;
        }
    }

    private void OnTowerCountModified()
    {
        if (TowerCount >= MaxTowers)
        {
            Activate();
        }
    }
}
