﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] TowerBehaviour prefab = default;

    void Start()
    {
        ObjectPooling.Ref.GetOrCreate(prefab, transform.position);
        Destroy(gameObject);
    }
}
