﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class FollowingLineRenderer : MonoBehaviour
{
    [SerializeField] Transform target = default;
    [SerializeField] int positionIndex = 1;

    LineRenderer lineRenderer;

    Vector3 TargetPosition => lineRenderer.useWorldSpace ? target.position : target.localPosition;

    private void Awake()
    {
        TryGetComponent(out lineRenderer);
    }

    private void LateUpdate()
    {
        lineRenderer.SetPosition(positionIndex, TargetPosition);
    }
}
