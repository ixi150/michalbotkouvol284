﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class DraggableObject : MonoBehaviour
{
    [SerializeField] Rigidbody2D holderRB = default;

    Rigidbody2D selfRB;
    Vector3 dragOffset;
    Camera cachedCamera;
    Transform cachedTransform;

    Vector3 MouseWorldPosition => cachedCamera.ScreenToWorldPoint(Input.mousePosition);

    private void Awake()
    {
        cachedTransform = transform;
        TryGetComponent(out selfRB);
    }

    private void Start()
    {
        cachedCamera = Camera.main;    
    }

    private void OnMouseDrag()
    {
        selfRB.position = MouseWorldPosition + dragOffset;
    }

    private void OnMouseDown()
    {
        dragOffset = cachedTransform.position - MouseWorldPosition;
        holderRB.simulated = false;
        selfRB.bodyType = RigidbodyType2D.Static;
    }

    private void OnMouseUp()
    {
        holderRB.simulated = true;
        selfRB.bodyType = RigidbodyType2D.Dynamic;

        selfRB.velocity = holderRB.velocity = Vector2.zero;
        selfRB.angularVelocity = holderRB.angularVelocity = 0f;
    }
}
